"use strict";

require("dotenv").config();
const fileupload = require("express-fileupload");
const cors = require("cors");
const express = require("express");
const morgan = require("morgan");
const app = express();
const port = process.env.SERVER_PORT || 3003;
const routerExperiencias = require("./app/routes/experiencias-routes");
const routerUsers = require("./app/routes/users-routes");
const routerComentarios = require("./app/routes/reviews-routes");
const fileUpload = require("express-fileupload");

app.use(cors());
app.use(express.static("public"));
app.use(fileupload());
app.use(express.json());

//Routes

app.use("/experiencias", routerExperiencias);
app.use("/users", routerUsers);
app.use("/comentarios", routerComentarios);

app.listen(port, () => console.log(`Escuchando puerto ${port}`));
