"use strict";

const jwt = require("jsonwebtoken");
const createJsonError = require("../app/errors/create-json-error");

async function validateAuth(req, res, next) {
  console.log("validate aut", req.body);
  try {
    const token = req.headers.authorization;
    const { JWT_SECRET } = process.env;
    console.log(token);
    const decodedToken = jwt.verify(token, JWT_SECRET);

    const { iduser, email } = decodedToken;

    req.auth = { iduser, email };
    next();
  } catch (error) {
    error.status = 401;
    createJsonError(error, res);
  }
}

module.exports = validateAuth;
