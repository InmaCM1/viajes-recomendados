"use strict";

const Joi = require("joi");
const {
  findComentariosByExperienciaId,
} = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");

const schema = Joi.number().positive();

async function comentarioByIdExperiencia(req, res) {
  try {
    const { idExperiencia } = req.params;
    await schema.validateAsync(idExperiencia);

    const comentario = await findComentariosByExperienciaId(idExperiencia);
    if (!comentario) {
      throwJsonError("Experiencia no existe", 400);
    }

    res.send(comentario);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = comentarioByIdExperiencia;
