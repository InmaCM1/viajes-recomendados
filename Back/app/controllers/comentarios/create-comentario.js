"use strict";

const Joi = require("joi");
const {
  findExperienciaById,
} = require("../../repositories/experiencia-repesitory");
const { addReview } = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

const schema = Joi.object().keys({
  comentario: Joi.string().min(5).max(255).required(),
  rating: Joi.number().min(0).max(10),
});

async function createReview(req, res) {
  try {
    console.log(req.auth);
    const { iduser } = req.auth;
    console.log("Este es el id", iduser);

    await schema.validateAsync(req.body);
    const { comentario, rating } = req.body;
    const { idexperiencia } = req.params;

    const experiencia = await findExperienciaById(idexperiencia);
    if (!experiencia) {
      throwJsonError("Experiencia no existe", 400);
    }

    const idReview = await addReview(iduser, idexperiencia, comentario, rating);

    res.status(200);
    res.send({ idReview, idexperiencia, comentario, rating });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = createReview;
