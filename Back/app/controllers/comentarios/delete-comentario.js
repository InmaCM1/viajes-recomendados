"use strict";

const Joi = require("joi");
const reviewsRepository = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");

const schema = Joi.number().positive().required();

async function deleteReview(req, res) {
  try {
    const { idcomentario } = req.params;
    const { iduser } = req.auth;
    await schema.validateAsync(idcomentario);

    const review = await reviewsRepository.findReviewById(idcomentario);

    if (!review) {
      const error = new Error("Review no existe");
      error.status = 400;
      throw error;
    }

    if (review.iduser !== iduser) {
      const error = new Error("No tienes permisos para realizar esta acción");
      error.status = 403;
      throw error;
    }

    await reviewsRepository.deleteReviewById(idcomentario);

    res.status(200);
    res.send({ message: `Review id:${idcomentario} borrada` });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = deleteReview;
