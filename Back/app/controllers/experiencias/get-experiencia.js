"use strict";
const createJsonError = require("../../errors/create-json-error");
const {
  findAllexperiencias,
} = require("../../repositories/experiencia-repesitory");

async function getExperiencias(req, res) {
  try {
    const experiencias = await findAllexperiencias();

    res.status(200);
    res.send(experiencias);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = {
  getExperiencias,
};
