"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const {
  findExperienciaById,
} = require("../../repositories/experiencia-repesitory");

const schema = Joi.number().integer().positive().required();

async function getExperienciaById(req, res) {
  try {
    const { idExperiencia } = req.params;
    await schema.validateAsync(idExperiencia);
    const experiencia = await findExperienciaById(idExperiencia);
    if (!experiencia) {
      throw new Error("Id no valido");
    }

    res.status(200);
    res.send(experiencia);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getExperienciaById };
