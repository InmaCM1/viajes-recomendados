"use strict";

const Joi = require("joi");
const throwJsonError = require("../../errors/throw-json-error");
const createJsonError = require("../../errors/create-json-error");
const {
  udpateExperiencias,
  findExperienciaById,
} = require("../../repositories/experiencia-repesitory");

const schema = Joi.object().keys({
  titulo: Joi.string().min(3).max(50).required(),
  lugar: Joi.string().min(2).max(50).required(),
  categoria: Joi.string().valid("mar", "montaña", "ciudad").required(),
  descripcion: Joi.string().min(10).max(400).required(),
});

async function updateExperiencia(req, res) {
  try {
    const { iduser } = req.auth;
    const { idexperiencias } = req.params;
    const { body } = req;
    const { titulo, lugar, categoria, descripcion } = body;
    console.log("el body de back", body);
    const experiencia = await findExperienciaById(idexperiencias);

    if (iduser !== experiencia.iduser) {
      throwJsonError(
        "Usuario no auotorizado para editar esta experiencia",
        500
      );
    }
    await schema.validateAsync(body);
    await udpateExperiencias({ ...body, idexperiencias });

    res.send({ titulo, lugar, categoria, descripcion });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { updateExperiencia };
