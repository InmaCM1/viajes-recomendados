"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");

const {
  findExperienciaById,
  removeExperienciaById,
} = require("../../repositories/experiencia-repesitory");
const schema = Joi.number().integer().positive().required();

async function deleteExperienciaById(req, res) {
  try {
    const { idExperiencia } = req.params;
    await schema.validateAsync(idExperiencia);

    const experiencia = await findExperienciaById(idExperiencia);
    if (!experiencia) {
      throw new Error("Experiencia no existe");
    }
    await removeExperienciaById(idExperiencia);
    //Con Mysql podremos lanzar un error si no existe el idCar;

    res.status(200);
    res.send({ mensaje: "experiencia borrada correctamente" });
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { deleteExperienciaById };
