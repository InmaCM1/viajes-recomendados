"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const {
  findExperienciaByLugar,
} = require("../../repositories/experiencia-repesitory");

const schema = Joi.string();

async function getExperienciaByLugar(req, res) {
  try {
    const { lugar } = req.query;
    await schema.validateAsync(lugar);
    console.log("lugar", lugar);
    const lugarExperiencia = await findExperienciaByLugar(lugar);
    if (!lugarExperiencia) {
      throw new Error("no hay ninguna experiencia por ese lugar");
    }

    res.status(200);
    res.send(lugarExperiencia);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getExperienciaByLugar };
