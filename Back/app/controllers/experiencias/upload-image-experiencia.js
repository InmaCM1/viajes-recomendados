"use strict";

const cryptoRandomString = require("crypto-random-string");
const createJsonError = require("../../errors/create-json-error");
const path = require("path");
const fs = require("fs");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findExperienciaImage,
  setExperienciaImage,
} = require("../../repositories/experiencia-repesitory");

const validExtensions = [".jpeg", ".jpg", ".png"];

async function uploadImageExperiencia(req, res) {
  try {
    // Obtenemos el id del JWT
    const { idexperiencias } = req.params;

    // Las imagenes vienen dentro de la cabecera req en el objeto files
    // Comprobamos q existe alguna imagen
    const { files } = req;
    if (!files || Object.keys(files).length === 0) {
      throwJsonError("No se ha seleccionado ningún fichero", 400);
    }

    // profileImage es el nombre que enviamos desde el postman,
    // si enviamos
    const { foto } = files;
    const extension = path.extname(foto.name);

    if (!validExtensions.includes(extension)) {
      throwJsonError("Formato no válido", 400);
    }

    const { HTTP_SERVER_DOMAIN, PATH_EXPERIENCIAS_IMAGE } = process.env;

    // Generamos la ruta completa a la carpeta donde situamos las imagenes de perfil
    const pathExperienciasImageFolder = `${__dirname}/../../../public/${PATH_EXPERIENCIAS_IMAGE}`;

    const random = cryptoRandomString({ length: 10, type: "alphanumeric" });
    const imageName = `${idexperiencias}-${random}${extension}`;
    // Path de la nueva imagen de perfil
    const pathImage = `${pathExperienciasImageFolder}/${imageName}`;
    //const pathImage = `${pathProfileImageFolder}/${id}${extension}`;

    // Movemos la image a la ruta final /public/images/profiles/14-adfa324d.png
    foto.mv(pathImage, async function (err) {
      if (err) return res.status(500).send(err);
      await setExperienciaImage(idexperiencias, imageName);

      res.send({
        url: `${HTTP_SERVER_DOMAIN}/${PATH_EXPERIENCIAS_IMAGE}/${imageName}`,
      });
    });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { uploadImageExperiencia };
