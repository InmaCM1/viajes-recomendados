"use strict";

const createJsonError = require("../../errors/create-json-error");
const {
  findExperienciasByUserId,
} = require("../../repositories/experiencia-repesitory");

async function getExperienciaByUserId(req, res) {
  try {
    const { iduser } = req.auth;
    const experienciaById = await findExperienciasByUserId(iduser);
    if (!experienciaById) {
      throw new Error("no hay ninguna experiencia por ese usuario");
    }

    res.status(200);
    res.send(experienciaById);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getExperienciaByUserId };
