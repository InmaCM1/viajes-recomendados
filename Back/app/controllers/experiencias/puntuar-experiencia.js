"use strict";

const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const {
  puntuarExperiencias,
} = require("../../repositories/experiencia-repesitory");

const schema = Joi.object().keys({
  puntuacion: Joi.string().min(1),
});

async function puntuarExperiencia(req, res) {
  try {
    const { body } = req;
    const { puntuacion } = body;
    const { idexperiencias } = req.params;

    await schema.validateAsync(body);
    await puntuarExperiencias(puntuacion, idexperiencias);

    res.send({ puntuacion });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { puntuarExperiencia };
