"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const { addExperiencia } = require("../../repositories/experiencia-repesitory");

const schema = Joi.object().keys({
  titulo: Joi.string().min(3).max(50).required(),
  lugar: Joi.string().min(2).max(50).required(),
  categoria: Joi.string().valid("mar", "montaña", "ciudad").required(),
  descripcion: Joi.string().min(1).max(400).required(),
});

async function createExperiencia(req, res) {
  try {
    const { iduser } = req.auth;
    const { body } = req;

    await schema.validateAsync(body);
    const idexperiencia = await addExperiencia({ ...body, iduser });

    res.status(201);
    res.send({ ...body, iduser, idexperiencia });
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { createExperiencia };
