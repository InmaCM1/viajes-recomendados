"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const {
  findExperienciaByCategoria,
} = require("../../repositories/experiencia-repesitory");

const schema = Joi.string().valid("mar", "montaña", "ciudad").required();

async function getExperienciaByCategoria(req, res) {
  try {
    const { categoria } = req.query;
    console.log("que sale de aqui", categoria);
    await schema.validateAsync(categoria);
    console.log("categoria", categoria);
    const categoriaExperiencia = await findExperienciaByCategoria(categoria);
    if (!categoriaExperiencia) {
      throw new Error("Categoria no valida");
    }

    res.status(200);
    res.send(categoriaExperiencia);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getExperienciaByCategoria };
