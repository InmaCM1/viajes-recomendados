"use strict";

const Joi = require("joi");
const bcrypt = require("bcryptjs");

const {
  getUserByEmail,
  udpateUserById,
} = require("../../repositories/user-repository");

const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

const schema = Joi.object().keys({
  nombreCompleto: Joi.string().min(3).max(20),
  email: Joi.string().email(),

  biografia: Joi.string().min(1).max(200).allow(null),
});

async function updateUser(req, res) {
  try {
    const { iduser } = req.auth;
    const { body } = req;
    await schema.validateAsync(body);
    const { nombreCompleto, email, biografia } = req.body;

    const user = await getUserByEmail(email);

    if (user && user.iduser !== iduser) {
      throwJsonError("Ya existe un usuario con ese email", 409);
    }

    const insertId = await udpateUserById({
      iduser: iduser,
      nombreCompleto: nombreCompleto,
      email: email,

      nombreCompleto: nombreCompleto,
      biografia: biografia,
    });

    res.send({
      iduser,
      nombreCompleto,
      email,
      nombreCompleto,
      biografia,
    });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { updateUser };
