"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const { findUserById } = require("../../repositories/user-repository");

const schema = Joi.number().integer().positive().required();

async function getUserById(req, res) {
  try {
    const { idUser } = req.params;
    await schema.validateAsync(idUser);
    const user = await findUserById(idUser);
    if (!user) {
      throw new Error("Id no valido");
    }

    res.status(200);
    res.send(user);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getUserById };
