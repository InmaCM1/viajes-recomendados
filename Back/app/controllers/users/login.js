"use strict";
const Joi = require("joi");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const createJsonError = require("../../errors/create-json-error");

const { getUserByEmail } = require("../../repositories/user-repository");

async function login(req, res) {
  try {
    const { body } = req;

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(2).max(45).required(),
    });

    schema.validateAsync(body);

    const { email, password } = body;
    const user = await getUserByEmail(email);
    console.log(user);

    if (!user) {
      throw new Error("No existe un usuario con ese email");
    }

    const { iduser, password: dbPassword } = user;

    const isValidPassword = await bcrypt.compare(password, dbPassword);

    if (!isValidPassword) {
      throw new Error("Contraseña incorrecta");
    }

    const { JWT_SECRET, JWT_SESSION_TIME } = process.env;
    const tokenPayload = { iduser, email };
    const token = jwt.sign(tokenPayload, JWT_SECRET, {
      expiresIn: JWT_SESSION_TIME,
    });

    console.log("token", token);

    const response = { accessToken: token, expiresIn: JWT_SESSION_TIME };
    res.status(200);
    res.send(response);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { login };
