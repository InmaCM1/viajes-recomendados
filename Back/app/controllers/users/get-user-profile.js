"use strict";

const Joi = require("joi");
const usersRepository = require("../../repositories/user-repository");
const createJsonError = require("../../errors/create-json-error");
const {
  findExperienciasByUserId,
} = require("../../repositories/experiencia-repesitory");

async function getUserProfile(req, res) {
  try {
    const { iduser } = req.auth;
    const user = await usersRepository.findUserById(iduser);
    const experiencias = await findExperienciasByUserId(iduser);

    const { nombreCompleto, email } = user;

    res.status(200);
    res.send({
      info: user,
      experiencias: experiencias,
    });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getUserProfile };
