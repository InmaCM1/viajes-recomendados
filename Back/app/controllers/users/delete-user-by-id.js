"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

const {
  findUserById,
  removeUserById,
} = require("../../repositories/user-repository");

const schema = Joi.number().integer().positive().required();

async function deleteUserById(req, res) {
  try {
    const { iduser } = req.auth;

    const user = await findUserById(iduser);

    if (iduser !== user.iduser) {
      throwJsonError("Usuario no auotorizado para borrar este usuario", 500);
    }

    await schema.validateAsync(iduser);
    await removeUserById(iduser);

    res.status(200);
    res.send({ mensaje: "El usuario se ha borrado correctamente" });
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { deleteUserById };
