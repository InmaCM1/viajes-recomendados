"use strict";

const cryptoRandomString = require("crypto-random-string");
const createJsonError = require("../../errors/create-json-error");
const path = require("path");
const fs = require("fs");
const {
  findUserProfileImage,
  uploadUserProfileImage,
} = require("../../repositories/user-repository");
const throwJsonError = require("../../errors/throw-json-error");

const validExtensions = [".jpeg", ".jpg", ".png"];

async function uploadImageProfile(req, res) {
  try {
    // Obtenemos el id del JWT
    const { iduser } = req.auth;

    // Las imagenes vienen dentro de la cabecera req en el objeto files
    // Comprobamos q existe alguna imagen
    const { files } = req;
    if (!files || Object.keys(files).length === 0) {
      throwJsonError("No se ha seleccionado ningún fichero", 400);
      // const error = new Error(message);
      // error.status = status;

      // throw error;
    }

    // profileImage es el nombre que enviamos desde el postman,
    // si enviamos
    const { profileImage } = files;
    const extension = path.extname(profileImage.name);

    if (!validExtensions.includes(extension)) {
      throwJsonError("Formato no valido", 400);
    }

    const { HTTP_SERVER_DOMAIN, PATH_USER_IMAGE } = process.env;
    // Cogemos la imagen de perfil original
    const user = await findUserProfileImage(iduser);
    // Generamos la ruta completa a la carpeta donde situamos las imagenes de perfil
    const pathProfileImageFolder = `${__dirname}/../../../public/${PATH_USER_IMAGE}`;

    // Borramos la imagen original si existe
    if (user.image) {
      await fs.unlink(`${pathProfileImageFolder}/${user.image}`, () => {
        console.log("Borrada imagen de perfil correctamente");
      });
    }

    const random = cryptoRandomString({ length: 10, type: "alphanumeric" });
    const imageName = `${iduser}-${random}${extension}`;
    // Path de la nueva imagen de perfil
    const pathImage = `${pathProfileImageFolder}/${imageName}`;
    //const pathImage = `${pathProfileImageFolder}/${id}${extension}`;

    // Movemos la image a la ruta final /public/images/profiles/14-adfa324d.png
    profileImage.mv(pathImage, async function (err) {
      if (err) return res.status(500).send(err);
      await uploadUserProfileImage(iduser, imageName);

      res.send({
        url: `${HTTP_SERVER_DOMAIN}/${PATH_USER_IMAGE}/${imageName}`,
      });
    });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { uploadImageProfile };
