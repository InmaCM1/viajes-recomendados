"use strict";
const Joi = require("joi");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const createJsonError = require("../../errors/create-json-error");

const {
  addUser,
  getUserByEmail,
} = require("../../repositories/user-repository");

async function register(req, res) {
  try {
    const { body } = req;

    const schema = Joi.object().keys({
      email: Joi.string().email().required(),
      password: Joi.string().min(2).max(45).required(),
      nombreCompleto: Joi.string().min(1).max(100).required(),
    });
    await schema.validateAsync(body);
    const { email, password, nombreCompleto } = body;
    const user = await getUserByEmail(email);

    if (user) {
      const error = new Error("Ya existe un usuario registrado con ese email");
      error.status = 400;
      throw error;
    }

    const contraseñaEncriptada = await bcrypt.hash(password, 5);
    console.log(contraseñaEncriptada);
    const insertId = await addUser({
      email: email,
      password: contraseñaEncriptada,
      nombreCompleto: nombreCompleto,
    });

    res.status(200);
    res.send({ insertId });
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { register };
