"use strict";

const express = require("express");

const validateAuth = require("../../middlewares/validate-auth");
const createComentario = require("../controllers/comentarios/create-comentario");
const deleteComentario = require("../controllers/comentarios/delete-comentario");
const comentarioByIdExperiencia = require("../controllers/comentarios/get-comentario-by-id-experiencia");

const router = express.Router();

//Privadas
router
  .route("/:idexperiencia/comment")
  .all(validateAuth)
  .post(createComentario);
router
  .route("/:idcomentario/delete")
  .all(validateAuth)
  .delete(deleteComentario);

router.route("/:idExperiencia").get(comentarioByIdExperiencia);

module.exports = router;
