"use strict";

const express = require("express");
const router = express.Router();

const validateAuth = require("../../middlewares/validate-auth");

const { getUserById } = require("../controllers/users/get-user-by-id");
const { deleteUserById } = require("../controllers/users/delete-user-by-id");
const { updateUser } = require("../controllers/users/update-user");
const { register } = require("../controllers/users/register");
const { login } = require("../controllers/users/login");
const {
  uploadImageProfile,
} = require("../controllers/users/upload-image-profile");
const { getUserProfile } = require("../controllers/users/get-user-profile");
const {
  getExperienciaByUserId,
} = require("../controllers/experiencias/get-experiencia-by-user-Id");

router.route("/register").post(register);
router.route("/login").post(login);
router.route("/profile").all(validateAuth).get(getUserProfile);
router.route("/profile/edit").all(validateAuth).put(updateUser);
router.route("/profile/uploadimage").all(validateAuth).post(uploadImageProfile);
router
  .route("/profile/summaryexperiences")
  .all(validateAuth)
  .get(getExperienciaByUserId);

router
  .route("/:idUser")
  .get(getUserById)
  .all(validateAuth)
  .delete(deleteUserById);
router.route("/").put(updateUser);
//router.route("/uploadphoto").put(uploadImageProfile);

module.exports = router;
