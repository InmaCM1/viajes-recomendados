"use strict";

const express = require("express");
const router = express.Router();
const validateAuth = require("../../middlewares/validate-auth");

const {
  getExperiencias,
} = require("../controllers/experiencias/get-experiencia");

const {
  getExperienciaById,
} = require("../controllers/experiencias/get-experiencia-by-Id");

const {
  getExperienciaByUserId,
} = require("../controllers/experiencias/get-experiencia-by-user-Id");
const {
  getExperienciaByCategoria,
} = require("../controllers/experiencias/get-experiencia-by-categoria");

const {
  getExperienciaByLugar,
} = require("../controllers/experiencias/get-experiencia-by-lugar");

const {
  createExperiencia,
} = require("../controllers/experiencias/create-experiencia");
const {
  deleteExperienciaById,
} = require("../controllers/experiencias/delete-experiencia-by-id");
const {
  updateExperiencia,
} = require("../controllers/experiencias/update-experiencia");

const {
  puntuarExperiencia,
} = require("../controllers/experiencias/puntuar-experiencia");

const {
  uploadImageExperiencia,
} = require("../controllers/experiencias/upload-image-experiencia");

router
  .route("/")
  .get(getExperiencias)
  .all(validateAuth)
  .post(createExperiencia);

router.route("/filterCategoria").get(getExperienciaByCategoria);
router.route("/filterLugar").get(getExperienciaByLugar);

router
  .route("/:idExperiencia")
  .get(getExperienciaById)
  .all(validateAuth)
  .delete(deleteExperienciaById);

router
  .route("/:idexperiencias/update")
  .all(validateAuth)
  .put(updateExperiencia);
router.route("/:idexperiencias/rate").all(validateAuth).put(puntuarExperiencia);

router.route("/:idexperiencias/uploadImage").post(uploadImageExperiencia);

module.exports = router;
