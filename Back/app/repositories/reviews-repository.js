"use strict";

const database = require("../infrastructure/database");

async function addReview(iduser, idexperiencia, comentario, rating) {
  const pool = await database.getPool();
  const now = new Date();
  const insertQuery = `INSERT
    INTO comentarios (iduser, idexperiencia, comentario, rating)
    VALUES (?, ?, ?, ?)`;
  const [created] = await pool.query(insertQuery, [
    iduser,
    idexperiencia,
    comentario,
    rating,
    now,
  ]);

  return created.insertId;
}

async function deleteReviewById(id) {
  const pool = await database.getPool();
  const query = "DELETE FROM comentarios WHERE idcomentarios = ?";
  const [reviews] = await pool.query(query, id);

  return reviews;
}

async function findReviewById(id) {
  const pool = await database.getPool();
  const query = "SELECT * FROM comentarios WHERE idcomentarios = ?";
  const [reviews] = await pool.query(query, id);

  return reviews[0];
}

async function findComentariosByExperienciaId(idExperiencia) {
  const pool = await database.getPool();
  console.log(idExperiencia);
  const query = `SELECT c.idcomentarios, u.fotoPerfil, u.nombreCompleto, c.comentario, c.rating FROM comentarios c INNER JOIN users u ON c.iduser=u.iduser INNER JOIN experiencias e ON c.idexperiencia=e.idexperiencias WHERE e.idexperiencias = ? `;
  const [reviews] = await pool.query(query, idExperiencia);
  console.log("que es este array", reviews);

  return reviews;
}

async function findReviewsByUserId(iduser) {
  const pool = await database.getPool();
  const query = `SELECT comentarios.* FROM comentarios 
  INNER JOIN users ON users.iduser = comentarios.iduser 
  WHERE users.iduser = ?`;

  const [reviews] = await pool.query(query, iduser);

  return reviews;
}

async function findAllReviews() {
  const pool = await database.getPool();
  const query = `SELECT comentarios.*, users.nombreCompleto, experiencias.titulo, experiencias.lugar, experiencias.categoria
    FROM comentarios
    INNER JOIN users ON users.iduser= comentarios.iduser
    INNER JOIN experiencias ON experiencias.idexperiencias = comentarios.iduser`;
  const [reviews] = await pool.query(query);

  return reviews;
}

async function getAverageRating(idExperiencia) {
  const pool = await database.getPool();
  const query =
    "SELECT AVG(rating) as valoracionMedia FROM comentarios WHERE idexperiencia = ?";
  const [valoracion] = await pool.query(query, idExperiencia);

  return valoracion[0].valoracionMedia;
}

async function getNumberOfRatings(idExperiencia) {
  const pool = await database.getPool();
  const query =
    "SELECT COUNT(rating) as numValoraciones FROM comentarios WHERE idexperiencia = ?";
  const [valoracion] = await pool.query(query, idExperiencia);

  return valoracion[0].numValoraciones;
}

module.exports = {
  addReview,
  deleteReviewById,
  findAllReviews,
  findReviewById,
  findComentariosByExperienciaId,
  findReviewsByUserId,
  getAverageRating,
  getNumberOfRatings,
};
