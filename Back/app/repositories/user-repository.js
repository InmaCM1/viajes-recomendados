"use strict";

const database = require("../infrastructure/database");

async function addUser(user) {
  const pool = await database.getPool();
  const consulta = `
    INSERT INTO users(
        email,
        password,
        nombreCompleto

    ) VALUES (?, ?, ?)
  `;
  const [created] = await pool.query(consulta, [...Object.values(user)]);

  return created.insertId;
}

async function findUserById(iduser) {
  const pool = await database.getPool();
  const consulta = "SELECT * FROM users WHERE iduser = ?";
  const [user] = await pool.query(consulta, iduser);

  return user[0];
}

async function removeUserById(iduser) {
  const pool = await database.getPool();
  const consulta = "DELETE FROM users WHERE iduser = ?";
  await pool.query(consulta, iduser);

  return true;
}

async function udpateUserById(data) {
  const { iduser, nombreCompleto, email, biografia, fotoPerfil } = data;
  const pool = await database.getPool();
  const updateQuery = `UPDATE users
  SET nombreCompleto = ?, email = ?, biografia = ?, fotoPerfil = ?
  WHERE iduser = ?`;
  await pool.query(updateQuery, [
    nombreCompleto,
    email,
    biografia,
    fotoPerfil,
    iduser,
  ]);

  return true;
}

async function getUserByEmail(email) {
  const pool = await database.getPool();
  const consulta = `SELECT iduser, email, password FROM users WHERE email= ?`;
  const [usuario] = await pool.query(consulta, email);

  return usuario[0];
}

async function findUserProfileImage(iduser) {
  const pool = await database.getPool();
  const query = "SELECT fotoPerfil FROM users WHERE iduser = ?";
  const [users] = await pool.query(query, iduser);

  return users[0];
}

async function uploadUserProfileImage(iduser, image) {
  const pool = await database.getPool();
  const updateQuery = "UPDATE users SET fotoPerfil = ? WHERE iduser = ?";
  await pool.query(updateQuery, [image, iduser]);

  return true;
}

module.exports = {
  addUser,
  findUserById,
  removeUserById,
  udpateUserById,
  getUserByEmail,
  findUserProfileImage,
  uploadUserProfileImage,
};
