"use strict";

const database = require("../infrastructure/database");

async function findAllexperiencias() {
  const pool = await database.getPool();
  const consulta =
    "SELECT e.*,MAX (f.foto) foto, ROUND(AVG(c.rating)) rating FROM experiencias e LEFT JOIN `fotos-viajes` f ON e.idexperiencias =f.idexperiencias LEFT JOIN comentarios c ON e.idexperiencias= c.idexperiencia GROUP BY e.idexperiencias HAVING foto IS NOT NULL ORDER BY rating DESC";
  const [experiencias] = await pool.query(consulta);

  return experiencias;
}

async function findExperienciaById(idexperiencias) {
  const pool = await database.getPool();
  const consulta =
    "SELECT e.*, MAX (f.foto) foto, ROUND (AVG (c.rating)) rating FROM experiencias e LEFT JOIN `fotos-viajes` f ON e.idexperiencias = f.idexperiencias LEFT JOIN comentarios c ON e.idexperiencias = c.idexperiencia WHERE e.idexperiencias = ?";
  const [experiencia] = await pool.query(consulta, idexperiencias);

  return experiencia[0];
}

async function findExperienciaByCategoria(categoria) {
  const pool = await database.getPool();
  const consulta =
    "SELECT e.*, MAX (f.foto) foto, ROUND(AVG(c.rating)) rating FROM experiencias e LEFT JOIN `fotos-viajes` f ON e.idexperiencias =f.idexperiencias LEFT JOIN comentarios c ON e.idexperiencias= c.idexperiencia WHERE categoria = ? GROUP BY e.idexperiencias";
  const [experiencia] = await pool.query(consulta, categoria);

  return experiencia;
}

async function findExperienciaByLugar(lugar) {
  const pool = await database.getPool();
  lugar = `%${lugar}%`;
  const consulta =
    "SELECT e.*, MAX (f.foto)foto, ROUND(AVG(c.rating)) rating FROM experiencias e LEFT JOIN `fotos-viajes` f ON e.idexperiencias =f.idexperiencias LEFT JOIN comentarios c ON e.idexperiencias= c.idexperiencia WHERE lugar LIKE ? GROUP BY e.idexperiencias ";

  const [experiencia] = await pool.query(consulta, lugar);

  console.log(experiencia);

  return experiencia;
}

async function findExperienciasByUserId(idUser) {
  const pool = await database.getPool();
  const query =
    "SELECT e.*, MAX (f.foto) foto, ROUND(AVG(c.rating)) rating FROM experiencias e LEFT JOIN `fotos-viajes` f ON e.idexperiencias =f.idexperiencias LEFT JOIN comentarios c ON e.idexperiencias= c.idexperiencia WHERE e.iduser= ? GROUP BY e.idexperiencias";

  const [experiencia] = await pool.query(query, idUser);

  return experiencia;
}

async function addExperiencia(experiencia) {
  const pool = await database.getPool();
  const now = new Date();
  const consulta = `INSERT INTO experiencias (
    titulo,
    lugar,
    categoria,
    descripcion,
    iduser
    ) VALUES (?, ?, ?, ?, ?)`;

  const [created] = await pool.query(consulta, [
    ...Object.values(experiencia),
    now,
  ]);

  return created.insertId;
}

async function removeExperienciaById(idExperiencia) {
  const pool = await database.getPool();
  const consulta = "DELETE FROM experiencias WHERE idexperiencias = ?";
  await pool.query(consulta, idExperiencia);

  return true;
}

async function udpateExperiencias(data) {
  const { idexperiencias, titulo, lugar, categoria, descripcion } = data;
  const pool = await database.getPool();
  const updateQuery = `UPDATE experiencias
  SET titulo = ?, lugar = ?, categoria = ?, descripcion = ?
  WHERE idexperiencias = ?`;
  await pool.query(updateQuery, [
    titulo,
    lugar,
    categoria,
    descripcion,
    idexperiencias,
  ]);

  return true;
}

async function setExperienciaImage(idexperiencias, foto) {
  const pool = await database.getPool();
  const updateQuery =
    "INSERT INTO `fotos-viajes` (idexperiencias, foto) VALUES (?,?)";
  await pool.query(updateQuery, [idexperiencias, foto]);

  return true;
}

async function findExperienciaImage(idexperiencias) {
  const pool = await database.getPool();
  const query = "SELECT foto FROM fotos-viajes WHERE idexperiencias = ?";
  const [users] = await pool.query(query, idexperiencias);

  return users[0];
}

async function puntuarExperiencias(puntuacion, idexperiencia) {
  const pool = await database.getPool();
  const puntua =
    "UPDATE experiencias SET puntuacion = ? WHERE idexperiencias = ?";
  await pool.query(puntua, [puntuacion, idexperiencia]);

  return true;
}

module.exports = {
  findAllexperiencias,
  findExperienciaById,
  findExperienciaByCategoria,
  addExperiencia,
  findExperienciaByLugar,
  removeExperienciaById,
  findExperienciasByUserId,
  udpateExperiencias,
  setExperienciaImage,
  findExperienciaImage,
  puntuarExperiencias,
};
