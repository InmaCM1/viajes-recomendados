import "./App.css";

import { TokenProvider } from "./components/TokenContext";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "../src/pages/HomePage";
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";

import EditProfilePage from "./pages/EditProfilePage";
import CreateExperienciaPage from "./pages/CreateExperienciaPage";
import SearchExperienciaPage from "./pages/SearchExperienciaPage";
import ShowExperienciaPage from "./pages/ShowExperienciaPage";
import SummaryUserExperiencesPage from "./pages/SummaryUserExperiencesPage";
import EditExperienciaPage from "./pages/EditExperienciaPage";

function App() {
  return (
    <Router>
      <TokenProvider>
        <div className="App">
          <Switch>
            <Route exact path="/">
              <HomePage></HomePage>
            </Route>
            <Route path="/register">
              <RegisterPage></RegisterPage>
            </Route>
            <Route path="/login">
              <LoginPage></LoginPage>
            </Route>
            <Route exact path="/profile/edit">
              <EditProfilePage></EditProfilePage>
            </Route>
            <Route exact path="/experiencias/:idexperiencia/editExperiencia">
              <EditExperienciaPage></EditExperienciaPage>
            </Route>
            <Route exact path="/profile/createExperiencia">
              <CreateExperienciaPage></CreateExperienciaPage>
            </Route>
            <Route exact path="/profile/summaryExperiencias">
              <SummaryUserExperiencesPage></SummaryUserExperiencesPage>
            </Route>
            <Route exact path="/experiencias/filter">
              <SearchExperienciaPage></SearchExperienciaPage>
            </Route>
            <Route exact path="/experiencia/:idexperiencia">
              <ShowExperienciaPage></ShowExperienciaPage>
            </Route>
          </Switch>
        </div>
      </TokenProvider>
    </Router>
  );
}

export default App;
