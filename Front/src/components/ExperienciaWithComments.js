import { SERVER_URL } from "../constants";
import CommentsList from "../components/CommentsList";
import decodeTokenData from "../helpers/decodeTokenData";
import { TokenContext } from "./TokenContext";
import { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { deleteExperienciasById } from "../api/api";
import "./experienciaWithComments.css";
import { Rating } from "@material-ui/lab";

function ExperienciaWithComments(props) {
  const {
    titulo,
    despcripcion,
    foto,
    rating,
    comentarios,
    experienciaUserId,
    idexperiencia,
  } = props;

  const history = useHistory();
  const [token] = useContext(TokenContext);
  const decodedToken = decodeTokenData(token);
  console.log("decode token", decodedToken);
  const deleteExperiencia = async (e) => {
    e.stopPropagation();
    await deleteExperienciasById(idexperiencia, token);
    history.push("/profile/summaryExperiencias");
  };

  return (
    <>
      <div className="experienciaInfo">
        <h2>{titulo}</h2>
        {foto && (
          <img src={`${SERVER_URL}/images/experiencias/${foto}`} alt={titulo} />
        )}
        <p>{despcripcion}</p>
        <Rating value={rating} readOnly={true}></Rating>
      </div>

      <div className="experienciaComments">
        <h3>Comentarios de los viajeros</h3>
        <CommentsList
          classname="lista_comentarios"
          comments={comentarios}
        ></CommentsList>

        {/*  {decodedToken?.iduser === experienciaUserId && (
          <>
            <Link to={`/experiencias/${idexperiencia}/editExperiencia`}>
              Editar experiencia
            </Link>
            <button onClick={deleteExperiencia}>Borrar experiencia</button>
          </>
        )}  */}
      </div>
    </>
  );
}

export default ExperienciaWithComments;
