import React from "react";
import logo from "../images/logoike.png";
import icon_register from "../images/icono_registro.png";
import "./header.css";
import { useState } from "react";
import { Link } from "react-router-dom";

function Header() {
  const [filtrar, setFiltrar] = useState("");

  return (
    <div className="header_menu">
      <a href="/" className="header_menu_logo ">
        <img src={logo} alt="imagen logo Ike" />
      </a>
      <div class="box">
        <form class="container-1">
          <input
            value={filtrar}
            onChange={(e) => setFiltrar(e.target.value)}
            type="search"
            id="search"
            placeholder="Encuentra tú próximo destino"
          />
          <span>
            <Link
              to={`experiencias/filter?lugar=${filtrar}`}
              class="fas fa-search"
            ></Link>
          </span>
        </form>
      </div>
      <div className="header_nav">
        <nav>
          <a href="/register">Regístrate</a>
        </nav>
      </div>

      <a className="header_register_icon" href="/login">
        <img src={icon_register} alt="Icono avatar registro" />
      </a>
    </div>
  );
}

export default Header;
