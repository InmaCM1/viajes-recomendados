import React from "react";
import { useState } from "react";
import { postRegister } from "../api/api";
import { useHistory } from "react-router-dom";
import "./registerForm.css";

export const RegisterForm = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [nombreCompleto, setNombre] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const handleRegister = async (e) => {
    e.preventDefault();

    const requestBody = {
      email,
      password,
      nombreCompleto,
    };

    const res = await postRegister(requestBody);

    if (res.ok) {
      setErrorMsg(null);
      history.push("/login");
    } else {
      setErrorMsg(`Error de registro`);
    }
  };

  return (
    <div class="ficha_registro">
      <h3 className="titulo_Registro">Registro nuevo usuario</h3>
      <form className="form_registro" onSubmit={handleRegister} id="register">
        <div>
          <label htmlFor="name">Nombre</label>
          <input
            type="text"
            name="name"
            value={nombreCompleto}
            onChange={(e) => setNombre(e.target.value)}
          />
        </div>

        <div>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div>
          <label htmlFor="password">Password</label>
          <input
            name="password"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <div>
          <button type="submit">Enviar</button>
        </div>
      </form>
      {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
    </div>
  );
};

export default RegisterForm;
