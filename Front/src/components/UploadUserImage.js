import { useRef } from "react";
import { postFileUserAvatar } from "../api/api";

const UploadUserImage = (props) => {
  const { token } = props;

  const fileInput = useRef();

  const fileUpload = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    console.log("hola3");

    const file = fileInput.current.files[0];
    formData.append("profileImage", file);
    const response = await postFileUserAvatar(formData, token);

    const body = await response.json();
    console.log("fileUpload", body);
  };

  return (
    <form onSubmit={fileUpload} id="subirImageUsuarioForm">
      <input type="file" ref={fileInput} accept="image/*"></input>
    </form>
  );
};
export default UploadUserImage;
