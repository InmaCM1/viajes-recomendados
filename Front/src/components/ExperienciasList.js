import Experiencia from "./Experiencia";

const ExperienciasList = (props) => {
  const { experiencias } = props;

  return (
    <ul className="experiencias">
      {experiencias.map((experiencia) => (
        <Experiencia
          key={experiencia.idexperiencias}
          idexperiencia={experiencia.idexperiencias}
          foto={experiencia.foto}
          titulo={experiencia.titulo}
          lugar={experiencia.lugar}
          categoria={experiencia.categoria}
          descripcion={experiencia.descripcion}
          experienciaUserId={experiencia.iduser}
          rating={experiencia.rating}
        />
      ))}
    </ul>
  );
};

export default ExperienciasList;
