import React from "react";
import { Link } from "react-router-dom";
import "./navigationProfile.css";
import logo from "../../images/logoike.png";

export const NavigationProfile = (props) => {
  return (
    <nav className="navigation_profile">
      <ul>
        <a href="/" className="header_menu_logo_profile">
          <img src={logo} alt="imagen logo Ike" />
        </a>

        <li>
          <Link to="/profile/edit">Mi perfil</Link>
        </li>
        <li>
          <Link to="/profile/createExperiencia">Publicar Experiencia</Link>
        </li>
        <li>
          <Link to="/profile/summaryExperiencias">Historial Experiencias</Link>
        </li>
      </ul>
    </nav>
  );
};
