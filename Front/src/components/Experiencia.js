import { SERVER_URL } from "../constants";
import { useHistory } from "react-router-dom";
import { TokenContext } from "./TokenContext";
import { useContext } from "react";
import decodeTokenData from "../helpers/decodeTokenData";
import { deleteExperienciasById } from "../api/api";
import { Rating } from "@material-ui/lab";
import "./experiencia.css";
function Experiencia(props) {
  const {
    foto,
    titulo,
    lugar,
    categoria,
    descripcion,
    rating,
    idexperiencia,
    experienciaUserId,
  } = props;
  const history = useHistory();
  const [token] = useContext(TokenContext);
  const decodedToken = decodeTokenData(token);
  console.log("decode token", decodedToken);
  const deleteExperiencia = async (e) => {
    e.stopPropagation();
    await deleteExperienciasById(idexperiencia, token);
    history.go();
  };

  const goToExperiencia = (e) => {
    e.stopPropagation();
    history.push(`/experiencia/${idexperiencia}`);
  };

  return (
    <div onClick={goToExperiencia} className="experiencia">
      {foto && (
        <img
          className="experiencia_img"
          src={`${SERVER_URL}/images/experiencias/${foto}`}
          alt={titulo}
        />
      )}
      <p className="titulo_experiencia_busqueda">{titulo}</p>
      <p>{lugar}</p>
      <p>{categoria}</p>
      <p>{descripcion}</p>
      <Rating value={rating} readOnly={true}></Rating>
      {decodedToken.iduser === experienciaUserId && (
        <button
          className="button_delete_experiencia"
          onClick={deleteExperiencia}
        >
          Borrar experiencia
        </button>
      )}
    </div>
  );
}

export default Experiencia;
