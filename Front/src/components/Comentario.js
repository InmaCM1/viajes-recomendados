import { SERVER_URL } from "../constants";
import "./comentario.css";
import { Rating } from "@material-ui/lab";

function Comentario(props) {
  const { fotoPerfil, nombre, comentario, rating } = props;

  return (
    <div className="comment">
      <img
        className="comment_img"
        src={`${SERVER_URL}/images/profiles/${
          fotoPerfil || "icono_registro.png"
        }`}
        alt={nombre}
      />
      <p>{nombre}</p>
      <p>{comentario}</p>
      <Rating value={rating} readOnly={true}></Rating>
    </div>
  );
}

export default Comentario;
