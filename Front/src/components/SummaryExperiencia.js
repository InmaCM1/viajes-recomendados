import { SERVER_URL } from "../constants";
import { Link } from "react-router-dom";
import "./summaryExperiencia.css";
import { Rating } from "@material-ui/lab";

function SummaryExperiencia(props) {
  const { id, titulo, despcripcion, foto, rating } = props;

  return (
    <div className="ficha_experiencia_SummaryPage">
      <h2 className="titulo_experiencia">{titulo}</h2>
      <img
        className="image_epxeriencia_summaryPage"
        src={`${SERVER_URL}/images/experiencias/${foto}`}
        alt={titulo}
      />
      <p>{despcripcion}</p>
      <div>
        <Rating value={rating} readOnly={true}></Rating>
      </div>
      <div className="boton_ver_mas">
        <Link to={`/experiencia/${id}`}>Ver más</Link>
      </div>
    </div>
  );
}

export default SummaryExperiencia;
