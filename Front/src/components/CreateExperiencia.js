import React from "react";
import { useState, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { postCreateExperiencia } from "../api/api";
import { useHistory } from "react-router-dom";
import UploadExperienciaImage from "../components/UploadExperienciaImage";
import "./CreateExperiencia.css";

export const CreateExperienciaForm = (props) => {
  const [token] = useContext(TokenContext);
  const history = useHistory();
  const [errorMsg, setErrorMsg] = useState(null);
  const [titulo, setTitulo] = useState("");
  const [lugar, setLugar] = useState("");
  const [categoria, setCategoria] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [idexperiencia, setIdexperiencia] = useState();

  const createExperiencia = async (e) => {
    e.preventDefault();
    const requestBody = {
      titulo,
      lugar,
      categoria,
      descripcion,
    };

    const res = await postCreateExperiencia(requestBody, token);
    const bodyDeLaRespuesta = await res.json();

    if (res.ok) {
      setIdexperiencia(bodyDeLaRespuesta.idexperiencia);
      setErrorMsg(null);
    } else {
      setErrorMsg(
        `No se ha podido crear una experiencia: ${bodyDeLaRespuesta}`
      );
    }
  };

  const publicarExperiencia = async (e) => {
    const button1 = document.createElement("input");
    const button2 = document.createElement("input");
    button1.style.display = "none";
    button2.style.display = "none";
    button1.type = "submit";
    button2.type = "submit";

    document
      .getElementById("crearExperienciaForm")
      .appendChild(button1)
      .click();

    await setTimeout(() => {
      document
        .getElementById("subirImageExperienciaForm")
        .appendChild(button2)
        .click();
      history.push(`/profile/summaryExperiencias`);
    }, 200);
  };

  return (
    <>
      <div class="ficha_crear_Experiencia">
        <h2 className="titulo_publicar_experiencia">Comparte tú experiencia</h2>
        <form
          className="fomulario_crear_experiencia"
          onSubmit={createExperiencia}
          id="crearExperienciaForm"
        >
          <div>
            <label htmlFor="titulo">- Titula tu experiencia -</label>
            <input
              type="text"
              name="titulo"
              value={titulo}
              onChange={(e) => setTitulo(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="lugar">- Destino -</label>
            <input
              type="text"
              name="lugar"
              value={lugar}
              onChange={(e) => setLugar(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="categoria">- Categoria -</label>
            <input
              name="categoria"
              type="text"
              value={categoria}
              onChange={(e) => setCategoria(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="descripcion">- Descripción -</label>
            <textarea
              className="contenedor_descripcion"
              type="text"
              name="descripcion"
              value={descripcion}
              onChange={(e) => setDescripcion(e.target.value)}
            />
          </div>
        </form>

        <p className="Titulo_foto_experiencia">- Sube un recuerdo -</p>

        {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}

        <div className="apartado_subir_foto_experiencia">
          <UploadExperienciaImage
            idexperiencia={idexperiencia}
          ></UploadExperienciaImage>
        </div>

        <div class="boton_crear_experiencia">
          <button onClick={publicarExperiencia} type="button">
            Publicar
          </button>
        </div>
      </div>
    </>
  );
};

export default CreateExperienciaForm;
