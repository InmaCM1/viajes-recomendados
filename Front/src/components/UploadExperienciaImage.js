import { useRef } from "react";
import { postFile } from "../api/api";

const UploadExperienciaImage = (props) => {
  const { idexperiencia } = props;
  console.log("componente", idexperiencia);
  const fileInput = useRef();

  const fileUpload = async (e) => {
    e.preventDefault();
    const formData = new FormData();

    const file = fileInput.current.files[0];
    formData.append("foto", file);
    const response = await postFile(formData, idexperiencia);

    const body = await response.json();
    console.log("fileUpload", body);
  };

  return (
    <form onSubmit={fileUpload} id="subirImageExperienciaForm">
      <input type="file" ref={fileInput} accept="image/*"></input>
    </form>
  );
};
export default UploadExperienciaImage;
