import React from "react";
import { useState, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { updateExperienciaById } from "../api/api";
import "./CreateExperiencia.css";
import { useHistory, useParams } from "react-router-dom";
import { useExperienciaById } from "../hooks/remoteHooks";
import "./editExperiencia.css";

export const CreateExperienciaForm = (props) => {
  const [token] = useContext(TokenContext);
  const history = useHistory();
  const [errorMsg, setErrorMsg] = useState(null);
  const [titulo, setTitulo] = useState("");
  const [lugar, setLugar] = useState("");
  const [categoria, setCategoria] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const { idexperiencia } = useParams();
  const [experiencia] = useExperienciaById(idexperiencia);
  console.log("experiencia", experiencia);
  const updateExperiencia = async (e) => {
    e.preventDefault();
    const requestBody = {
      titulo: titulo || experiencia.titulo,
      lugar: lugar || experiencia.lugar,
      descripcion: descripcion || experiencia.descripcion,
      categoria: categoria || experiencia.categoria,
    };
    console.log("el body", requestBody);
    const res = await updateExperienciaById(idexperiencia, token, requestBody);

    if (res.ok) {
      history.go();
    } else {
      setErrorMsg(`No se ha podido editar la experiencia`);
    }
  };

  return (
    <>
      <h2 className="titulo_publicar_experiencia">Comparte tú experiencia</h2>
      <div class="edit_Experiencia">
        <form
          classname="fomulario_edit_experiencia"
          onSubmit={updateExperiencia}
          id="crearExperienciaForm"
        >
          <div>
            <label htmlFor="titulo">Titula tu experiencia</label>
            <input
              type="text"
              placeholder={experiencia?.titulo}
              name="titulo"
              value={titulo}
              onChange={(e) => setTitulo(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="lugar">Destino</label>
            <input
              type="text"
              placeholder={experiencia?.lugar}
              name="lugar"
              value={lugar}
              onChange={(e) => setLugar(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="categoria">Categoria</label>
            <input
              name="categoria"
              type="text"
              placeholder={experiencia?.categoria}
              value={categoria}
              onChange={(e) => setCategoria(e.target.value)}
            />
          </div>

          <div>
            <label htmlFor="descripcion">Descripción</label>
            <input
              type="text"
              placeholder={experiencia?.descripcion}
              name="descripcion"
              value={descripcion}
              onChange={(e) => setDescripcion(e.target.value)}
            />
          </div>
          <div class="boton_edit_experiencia">
            <input type="submit" value="Guardar cambios"></input>
          </div>
        </form>
        {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
      </div>
    </>
  );
};

export default CreateExperienciaForm;
