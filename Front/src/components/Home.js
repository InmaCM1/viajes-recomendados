import React from "react";
import "./home.css";
import SummaryExperiencia from "../components/SummaryExperiencia";
import { useExperienciasByRating } from "../hooks/remoteHooks";

function Home() {
  const [experiencias] = useExperienciasByRating();
  return (
    <div className="home">
      <div className="home_title">
        <h2>Si te gusta viajar...</h2>
      </div>
      <div className="home__content">
        {experiencias.length > 0 && (
          <>
            <SummaryExperiencia
              id={experiencias[0].idexperiencias}
              titulo={experiencias[0].titulo}
              despcripcion={experiencias[0].despcripcion}
              rating={experiencias[0].rating}
              foto={experiencias[0].foto}
            ></SummaryExperiencia>

            <SummaryExperiencia
              id={experiencias[1].idexperiencias}
              titulo={experiencias[1].titulo}
              despcripcion={experiencias[1].despcripcion}
              rating={experiencias[1].rating}
              foto={experiencias[1].foto}
            ></SummaryExperiencia>

            <SummaryExperiencia
              id={experiencias[2].idexperiencias}
              titulo={experiencias[2].titulo}
              despcripcion={experiencias[2].despcripcion}
              rating={experiencias[2].rating}
              foto={experiencias[2].foto}
            ></SummaryExperiencia>
          </>
        )}
      </div>
    </div>
  );
}

export default Home;
