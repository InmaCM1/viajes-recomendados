import React from "react";
import logo from "../images/logoike.png";
import "./headerProfile.css";

function HeaderProfile() {
  return (
    <div className="header_menu_profile">
      <a href="/" className="header_menu_logo_profile ">
        <img src={logo} alt="imagen logo Ike" />
      </a>
    </div>
  );
}

export default HeaderProfile;
