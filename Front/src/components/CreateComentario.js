import React from "react";
import { useState, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { postCreateComentario } from "../api/api";
import { useHistory } from "react-router-dom";
import "./createcomentario.css";
import { Rating } from "@material-ui/lab";

export const CreateComentario = (props) => {
  const { idexperiencia } = props;
  const [token] = useContext(TokenContext);
  const [errorMsg, setErrorMsg] = useState(null);
  const [comentario, setComentario] = useState("");
  const [rating, setRating] = useState("");
  const history = useHistory();

  const crearComentario = async (e) => {
    e.preventDefault();
    const requestBody = {
      comentario,
      rating,
    };

    const res = await postCreateComentario(idexperiencia, requestBody, token);
    const bodyDeLaRespuesta = await res.json();

    if (res.ok) {
      setErrorMsg(null);
      history.go();
    } else {
      setErrorMsg(`No se ha podido obtener el token: ${bodyDeLaRespuesta}`);
    }
  };

  return (
    <div class="ficha_create_comentario">
      <h3 className="titulo_create_comentario">Publica tú comentario</h3>
      <form
        className="form_create_comentario"
        onSubmit={crearComentario}
        id="editComentario"
      >
        <div>
          <label htmlFor="comentario">Comentario</label>
          <textarea
            type="text"
            name="comentario"
            value={comentario}
            onChange={(e) => setComentario(e.target.value)}
          />
        </div>

        <div>
          <label htmlFor="rating">Puntuación</label>
          <div className="rating">
            <Rating
              value={rating}
              onChange={(e) => setRating(e.target.value)}
            ></Rating>
          </div>
        </div>

        <div>
          <button className="button_publicar_comentario" type="submit">
            Publicar
          </button>
        </div>
      </form>
      {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
    </div>
  );
};

export default CreateComentario;
