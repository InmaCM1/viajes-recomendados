import React from "react";
import { useState, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { deleteExperienciasById } from "../api/api";

export const DeleteExperiencia = (props) => {
  const [, setToken] = useContext(TokenContext);
  const [errorMsg, setErrorMsg] = useState(null);
  const [experiencia, setExperiencia] = useState("");

  const handleRegister = async (e) => {
    e.preventDefault();
    const requestBody = {
      experiencia,
    };

    const res = await deleteExperienciasById(requestBody);
    const bodyDeLaRespuesta = await res.json();

    if (res.ok) {
      setToken(bodyDeLaRespuesta.accessToken);
      setErrorMsg(null);
    } else {
      setErrorMsg(`No se ha podido obtener el token: ${bodyDeLaRespuesta}`);
    }
  };

  return (
    <div>
      <form
        class="Delete_Experiencia"
        onSubmit={handleRegister}
        id="editExperiencia"
      >
        <div>
          <div class="button_delete_experiencia">
            <input
              type="submit"
              value="Borrar"
              onClick={(e) => setExperiencia(e.target.value)}
            >
              {" "}
              Borrar Experiencia
            </input>
          </div>
        </div>
      </form>
      {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
    </div>
  );
};

export default DeleteExperiencia;
