import React from "react";
import "./loginForm.css";
import { useContext, useState } from "react";
import { TokenContext } from "./TokenContext";
import { Redirect } from "react-router-dom";

const LoginForm = (props) => {
  const [token, setToken] = useContext(TokenContext);
  const [errorMsg, setErrorMsg] = useState(null);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = async (e) => {
    e.preventDefault();

    const res = await fetch("http://localhost:8080/users/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    });

    if (res.ok) {
      const body = await res.json();
      setToken(body.accessToken);
    } else {
      setErrorMsg("Email o contraseña incorrectos");
    }
  };
  return (
    <>
      {token ? (
        <Redirect to="/profile/edit"></Redirect>
      ) : (
        <div className="ficha_login">
          <h2 className="titulo_login">Login</h2>
          <form className="form_login" onSubmit={handleLogin}>
            <div>
              <label htmlFor="email">Email</label>
              <input
                name="email"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div>
              <label htmlFor="password">Contraseña</label>
              <input
                name="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div>
              <button type="submit">Enviar</button>
            </div>
            {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
          </form>
        </div>
      )}
    </>
  );
};

export default LoginForm;
