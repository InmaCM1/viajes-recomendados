import React from "react";
import { useState, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { postEditProfile } from "../api/api";
import { useGetUserProfile } from "../hooks/remoteHooks";
import "./editProfile.css";
import UploadUserImage from "../components/UploadUserImage";
import { useHistory } from "react-router-dom";
import { SERVER_URL } from "../constants";

export const ProfileForm = (props) => {
  const [token, setToken] = useContext(TokenContext);
  const [errorMsg, setErrorMsg] = useState(null);
  const [nombreCompleto, setNombre] = useState("");
  const [email, setEmail] = useState("");
  const [biografia, setBiografia] = useState("");
  const [user, setUser] = useGetUserProfile(token);
  const history = useHistory();

  const updateUser = async (e) => {
    e.preventDefault();
    const requestBody = {
      email: email || user.info.email,
      nombreCompleto: nombreCompleto || user.info.nombreCompleto,
      biografia: biografia || user.info.biografia,
    };

    const res = await postEditProfile(requestBody, token);
    const bodyDeLaRespuesta = await res.json();
    console.log("aquí el body", bodyDeLaRespuesta);
    if (res.ok) {
      setUser({ info: bodyDeLaRespuesta });
      setNombre("");
      setEmail("");
      setBiografia("");
      setErrorMsg(null);
    } else {
      setErrorMsg(`No se ha podido editar el perfil`);
    }
  };

  const guardarCambios = async (e) => {
    const button1 = document.createElement("input");
    const button2 = document.createElement("input");
    button1.style.display = "none";
    button2.style.display = "none";
    button1.type = "submit";
    button2.type = "submit";

    document.getElementById("editProfile").appendChild(button1).click();

    await setTimeout(() => {
      document
        .getElementById("subirImageUsuarioForm")
        .appendChild(button2)
        .click();

      history.go();
    }, 200);
  };

  return (
    <div class="ficha_editar_perfil">
      <h3 className="titulo_editProfile">Edita tú perfil</h3>

      <form className="form_editProfile" onSubmit={updateUser} id="editProfile">
        <div>
          <label htmlFor="name">- Nombre -</label>
          <input
            type="text"
            placeholder={user?.info.nombreCompleto}
            name="name"
            value={nombreCompleto}
            onChange={(e) => setNombre(e.target.value)}
          />
        </div>

        <div>
          <label htmlFor="email">- Email -</label>
          <input
            type="email"
            placeholder={user?.info.email}
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div>
          <label htmlFor="name">- Biografia -</label>
          <textarea
            className="contenedor_biografia"
            type="text"
            placeholder={user?.info.biografia}
            name="biografia"
            value={biografia}
            onChange={(e) => setBiografia(e.target.value)}
          />
        </div>
      </form>

      <div className="titulo_Foto_perfil">
        - Foto perfil -
        <img
          className="profile_img"
          src={`${SERVER_URL}/images/profiles/${
            user?.info.fotoPerfil || "defaultavatar.png"
          }`}
          alt={user?.info.nombreCompleto}
        />
      </div>

      <div className="apartado_subir_foto_avatar">
        <UploadUserImage token={token}></UploadUserImage>
      </div>

      <div>
        <button
          onClick={guardarCambios}
          className="boton_editar_perfil"
          type="button"
        >
          Guardar cambios
        </button>
      </div>
      {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
    </div>
  );
};

export default ProfileForm;
