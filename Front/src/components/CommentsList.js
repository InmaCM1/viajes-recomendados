import Comentario from "./Comentario";
import "./commentList.css";

const CommentsList = (props) => {
  const { comments } = props;

  return (
    <ul classname="lista_comentarios">
      {comments.map((comment) => (
        <Comentario
          key={comment.idcomentarios}
          fotoPerfil={comment.fotoPerfil}
          nombreCompleto={comment.nombreCompleto}
          comentario={comment.comentario}
          rating={comment.rating}
        />
      ))}
    </ul>
  );
};

export default CommentsList;
