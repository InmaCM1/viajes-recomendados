import { SERVER_URL } from "../constants";

const postRegister = (requestBody) => {
  return fetch(`${SERVER_URL}/users/register`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(requestBody),
  });
};

const postEditProfile = (requestBody, token) => {
  console.log(token);
  return fetch(`${SERVER_URL}/users/profile/edit`, {
    method: "PUT",
    headers: {
      Authorization: token,
      "Content-type": "application/json",
    },
    body: JSON.stringify(requestBody),
  });
};

const postCreateExperiencia = (requestBody, token) => {
  return fetch(`${SERVER_URL}/experiencias/`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      Authorization: token,
    },
    body: JSON.stringify(requestBody),
  });
};

const postCreateComentario = (idexperiencia, requestBody, token) => {
  return fetch(`${SERVER_URL}/comentarios/${idexperiencia}/comment`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      Authorization: token,
    },
    body: JSON.stringify(requestBody),
  });
};

const getFiltroExperiencias = (lugar) => {
  return fetch(`${SERVER_URL}/experiencias/filterLugar?lugar=${lugar}`);
};

const getPickExperiencia = (idexperiencia) => {
  return fetch(`${SERVER_URL}/experiencias/${idexperiencia}`);
};

const getCommentsByExperienciaId = (idexperiencia) => {
  return fetch(`${SERVER_URL}/comentarios/${idexperiencia}`);
};

const getExperiencesByUserId = (token) => {
  return fetch(`${SERVER_URL}/users/profile/summaryexperiences`, {
    headers: {
      Authorization: token,
    },
  });
};

const postFile = (requestBody, idexperiencia) => {
  return fetch(`${SERVER_URL}/experiencias/${idexperiencia}/uploadImage`, {
    method: "POST",
    body: requestBody,
  });
};

const postFileUserAvatar = (requestBody, token) => {
  return fetch(`${SERVER_URL}/users/profile/uploadimage`, {
    method: "POST",
    headers: {
      Authorization: token,
    },
    body: requestBody,
  });
};

const deleteExperienciasById = (idexperiencia, token) => {
  return fetch(`${SERVER_URL}/experiencias/${idexperiencia}`, {
    method: "DELETE",
    headers: {
      Authorization: token,
    },
  });
};

const getUserProfile = (token) => {
  return fetch(`${SERVER_URL}/users/profile`, {
    headers: { Authorization: token },
  });
};

const getExperienciasByRating = () => {
  return fetch(`${SERVER_URL}/experiencias`);
};

const updateExperienciaById = (idexperiencia, token, requestBody) => {
  return fetch(`${SERVER_URL}/experiencias/${idexperiencia}/update`, {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
      Authorization: token,
    },
    body: JSON.stringify(requestBody),
  });
};

export {
  postRegister,
  postEditProfile,
  postCreateExperiencia,
  getFiltroExperiencias,
  postCreateComentario,
  getPickExperiencia,
  getCommentsByExperienciaId,
  getExperiencesByUserId,
  postFile,
  deleteExperienciasById,
  getUserProfile,
  getExperienciasByRating,
  postFileUserAvatar,
  updateExperienciaById,
};
