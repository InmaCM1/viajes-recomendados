import { NavigationProfile } from "../components/Navigation/navigationProfile";
import EditExperiencia from "../components/EditExperiencia";

function EditExperienciaPage() {
  return (
    <div>
      <NavigationProfile />
      <EditExperiencia></EditExperiencia>
    </div>
  );
}

export default EditExperienciaPage;
