import HeaderProfile from "../components/HeaderProfile";
import Footer from "../components/Footer";
import RegisterForm from "../components/RegisterForm";

function RegisterPage() {
  return (
    <div>
      <HeaderProfile></HeaderProfile>
      <RegisterForm></RegisterForm>
    </div>
  );
}

export default RegisterPage;
