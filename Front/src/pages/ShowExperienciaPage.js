import Footer from "../components/Footer";
import { useParams } from "react-router-dom";
import {
  useExperienciaById,
  useComentarioByExperienciaId,
} from "../hooks/remoteHooks";
import ExperienciaWithComments from "../components/ExperienciaWithComments";
import CreateComentario from "../components/CreateComentario";
import HeaderProfile from "../components/HeaderProfile";

function ShowExperienciaPage() {
  const { idexperiencia } = useParams();

  const [experiencia] = useExperienciaById(idexperiencia);
  const [comentarios] = useComentarioByExperienciaId(idexperiencia);
  console.log("aquí comentarios", comentarios);
  return (
    <div>
      <HeaderProfile></HeaderProfile>

      {experiencia && (
        <>
          <ExperienciaWithComments
            titulo={experiencia.titulo}
            despcripcion={experiencia.descripcion}
            foto={experiencia.foto}
            rating={experiencia.rating}
            comentarios={comentarios}
            experienciaUserId={experiencia.iduser}
            idexperiencia={idexperiencia}
          ></ExperienciaWithComments>
          <CreateComentario idexperiencia={idexperiencia}></CreateComentario>
        </>
      )}
    </div>
  );
}

export default ShowExperienciaPage;
