import Footer from "../components/Footer";
import { useLocation } from "react-router-dom";
import { useRemoteFilterExperiencias } from "../hooks/remoteHooks";
import Header from "../components/HeaderProfile";
import ExperienciasList from "../components/ExperienciasList";

function SearchExperiencia() {
  const useQuery = () => new URLSearchParams(useLocation().search);
  let query = useQuery();
  console.log(query.get("lugar"));
  const [experienciasList] = useRemoteFilterExperiencias(query.get("lugar"));
  console.log(experienciasList);
  return (
    <div>
      <Header></Header>
      <ExperienciasList experiencias={experienciasList}></ExperienciasList>
      <Footer></Footer>
    </div>
  );
}

export default SearchExperiencia;
