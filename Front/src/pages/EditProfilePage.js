import Footer from "../components/Footer";
import EditProfile from "../components/EditProfile";
import { NavigationProfile } from "../components/Navigation/navigationProfile";

function EditProfilePage() {
  return (
    <div>
      <NavigationProfile />
      <EditProfile></EditProfile>
    </div>
  );
}

export default EditProfilePage;
