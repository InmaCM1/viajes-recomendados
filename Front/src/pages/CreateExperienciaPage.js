import CreateExperiencia from "../components/CreateExperiencia";
import { NavigationProfile } from "../components/Navigation/navigationProfile";

function CreateExperienciaPage() {
  return (
    <div>
      <NavigationProfile />
      <CreateExperiencia></CreateExperiencia>
    </div>
  );
}

export default CreateExperienciaPage;
