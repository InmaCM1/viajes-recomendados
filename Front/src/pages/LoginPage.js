import HeaderProfile from "../components/HeaderProfile";
import LoginForm from "../components/LoginForm";

function LoginPage() {
  return (
    <div>
      <HeaderProfile></HeaderProfile>
      <LoginForm></LoginForm>
    </div>
  );
}

export default LoginPage;
