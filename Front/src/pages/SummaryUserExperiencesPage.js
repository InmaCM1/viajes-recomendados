import { NavigationProfile } from "../components/Navigation/navigationProfile";
import {
  useExperiencesByUserId,
  /* useDeleteExperienciaByUser, */
} from "../hooks/remoteHooks";
import { DeleteExperiencia } from "../components/DeleteExperiencia";
import ExperienciasList from "../components/ExperienciasList";
import Footer from "../components/Footer";
import { useContext } from "react";
import { TokenContext } from "../components/TokenContext";

function SummaryUserExperiencesPage() {
  const [token] = useContext(TokenContext);
  const [experiencias] = useExperiencesByUserId(token);
  console.log("experiencias", experiencias);

  /* const [detedExperiencias] = useDeleteExperienciaByUser(token); */
  return (
    <div>
      <NavigationProfile />

      <ExperienciasList experiencias={experiencias}></ExperienciasList>
      {/* <DeleteExperiencia></DeleteExperiencia> */}
      <Footer></Footer>
    </div>
  );
}

export default SummaryUserExperiencesPage;
