import Header from "../components/Header";

import Home from "../components/Home";

function HomePage() {
  return (
    <div>
      <Header></Header>
      <Home></Home>
    </div>
  );
}

export default HomePage;
