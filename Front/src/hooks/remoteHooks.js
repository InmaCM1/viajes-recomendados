import { useContext, useEffect, useState } from "react";
import { TokenContext } from "../components/TokenContext";
import {
  getCommentsByExperienciaId,
  getFiltroExperiencias,
  getPickExperiencia,
  getExperiencesByUserId,
  deleteExperienciasById,
  getUserProfile,
  getExperienciasByRating,
} from "../api/api";

function useRemoteFilterExperiencias(lugar) {
  let [experienciaList, setExperienciaList] = useState([]);

  useEffect(() => {
    const cargarExperiencias = async () => {
      try {
        const respuesta = await getFiltroExperiencias(lugar);
        if (respuesta.ok) {
          const body = await respuesta.json();
          setExperienciaList(body);
        } else {
          setExperienciaList([]);
        }
      } catch (msg) {
        console.error(msg);
      }
    };
    const intervalo = setInterval(cargarExperiencias, 1000000);
    cargarExperiencias();
    return () => {
      clearInterval(intervalo);
    };
  }, []);

  return [experienciaList, setExperienciaList];
}

function useExperienciaById(idexperiencia) {
  const [experiencia, setExperiencia] = useState();
  useEffect(() => {
    const getExperiencia = async () => {
      const res = await getPickExperiencia(idexperiencia);
      const body = await res.json();
      setExperiencia(body);
    };
    getExperiencia();
  }, [idexperiencia]);
  return [experiencia, setExperiencia];
}

function useComentarioByExperienciaId(idexperiencia) {
  const [comentarios, setComentarios] = useState([]);
  useEffect(() => {
    const getComentarios = async () => {
      const res = await getCommentsByExperienciaId(idexperiencia);
      const body = await res.json();
      setComentarios(body);
    };
    getComentarios();
  }, [idexperiencia]);
  return [comentarios, setComentarios];
}

function useExperiencesByUserId(token) {
  const [experiencias, setExperiencias] = useState([]);
  useEffect(() => {
    const getExperiencesUser = async () => {
      const res = await getExperiencesByUserId(token);
      const body = await res.json();
      console.log("body", body);
      setExperiencias(body);
    };
    getExperiencesUser();
  }, [token]);
  return [experiencias, setExperiencias];
}

function useDeleteExperienciaByUser(iduser) {
  const [experienciaList, setExperienciaList] = useState([]);

  useEffect(() => {
    const cargarExperienciasByUser = async () => {
      try {
        const respuesta = await deleteExperienciasById(iduser);
        if (respuesta.ok) {
          const body = await respuesta.json();
          setExperienciaList(body);
        } else {
          setExperienciaList([]);
        }
      } catch (msg) {
        console.error(msg);
      }
    };
    const intervalo = setInterval(cargarExperienciasByUser, 1000000);
    cargarExperienciasByUser();
    return () => {
      clearInterval(intervalo);
    };
  }, []);

  return [experienciaList, setExperienciaList];
}

function useGetUserProfile(token) {
  const [userProfile, setUserProfile] = useState(null);

  useEffect(() => {
    const getExperiencesUser = async () => {
      const res = await getUserProfile(token);
      const body = await res.json();
      setUserProfile(body);
    };
    getExperiencesUser();
  }, [token]);
  console.log("y esto", userProfile);
  return [userProfile, setUserProfile];
}

function useExperienciasByRating() {
  const [experiencias, setExperiencias] = useState([]);

  useEffect(() => {
    const cargarExperienciasByRating = async () => {
      const res = await getExperienciasByRating();
      const body = await res.json();
      setExperiencias(body);
    };
    cargarExperienciasByRating();
  }, []);

  return [experiencias, setExperiencias];
}

export {
  useRemoteFilterExperiencias,
  useExperienciaById,
  useComentarioByExperienciaId,
  useExperiencesByUserId,
  useDeleteExperienciaByUser,
  useGetUserProfile,
  useExperienciasByRating,
};
