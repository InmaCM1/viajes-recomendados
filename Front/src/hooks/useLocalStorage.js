import { useEffect, useState } from "react";

export const useLocalStorage = (key, defaultValue) => {
  // preparamos el valor inicial:
  // Si hay algo en el localStorage con esta key, usamos eso
  //    - Como hemos guardado con JSON.stringify hay que hacer JSON.parse
  // Si no, usamos defaultValue
  const initialValue = localStorage.getItem(key)
    ? JSON.parse(localStorage.getItem(key))
    : defaultValue;

  // Preparamos un estado data
  const [data, setData] = useState(initialValue);

  // Monitorizamos el estado data para que cuando cambie
  // grabemos su nuevo valor en localstorage usando JSON.stringify
  // para que este hook sea genérico
  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(data));
  }, [data, key]);
  return [data, setData];
};
